import io

from fastapi import APIRouter, UploadFile, File
from pandas.io.excel import read_excel
from db import DbManager
from main import logger


imports_routes = APIRouter()

@imports_routes.post('/imports')
async def ImportsApiView( file: UploadFile = File(...) ):
    db = DbManager.connect()

    with io.BytesIO(file.file.read()) as fh:
        df = read_excel(fh)

    logger.info('Importado com sucesso')

    return {'Hello': 'Worlddd'}