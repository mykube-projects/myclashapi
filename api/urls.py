from main import app
from api.views import imports_routes

app.include_router(imports_routes, tags=['Importações'])