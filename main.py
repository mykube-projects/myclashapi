from typing import Optional
from fastapi import FastAPI
from settings import get_settings
from logger import logging, logger

settings = get_settings()
app = FastAPI()

from api.urls import *
