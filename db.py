import os

from pymongo import MongoClient
from main import settings as s
from main import logger

class DbManager:

    @staticmethod
    def connect():
        db = MongoClient(
            f'mongodb://{s.username}:{s.password}@{s.host}/{s.database}&authMechanism=SCRAM-SHA-256')

        logger.info(f'Connectado com sucesso {db}')

        return db